from tkinter import *
from fonctions_pythonOmg import *
import datetime
import random
import sys

"""
!!!!!!!!!!!Attention, ceci marche pour toutes les fonctions en dessous!!!!!!!!!!!

rows : nombres de lignes du canvas (en terme de rectangles)
cols : nombre de colonnes du canvas (en terme de rectangles)
cell_size : taille d'un rectangle du tableau
afforestation : pourcentage de foret sur le canvas
temps_ms : temps entre chaque etapes de l'automate
regle : choix entre la regle 1 ou 2 de l'ennonce quant a la mise en feu d'une parcelle de foret
rectangle : tableau regroupant les rectangles du canvas
rectangle_color : tableau regroupant les couleurs des rectangles du canvas
canvas : le canvas sur lequel sera affiche les rectangles

"""

def init_foret(rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas):
    """
    Permet, via un pourcentage, de choisir le taux de foret initiaux sur le canvas
    largeur : la largeur du canvas (en terme de rectangles)
    longueur : la longueur du canvas (en terme de rectangles)
    rand : un nombre random utile a la probabilite d'avoir une parcelle foret
    """
    for largeur in range (0,cols):
        for longueur in range (0,rows):
            rand = random.randint(1,100)
            if rand/100 <= afforestation:
                rectangle[largeur][longueur] = canvas.create_rectangle(largeur*cell_size, longueur*cell_size, largeur*cell_size + cell_size, longueur*cell_size + cell_size, fill="green")
                rectangle_color[largeur][longueur] = "green"
            else:
                rectangle[largeur][longueur] = canvas.create_rectangle(largeur*cell_size, longueur*cell_size, largeur*cell_size + cell_size, longueur*cell_size + cell_size, fill="white")
                rectangle_color[largeur][longueur] = "white"



def fonction_clic(rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,event,canvas):
    """
    Lorsque le clic gauche de la souris est appuyee sur un rectangle du canvas, provoque une mise en feu de la parcelle appuyee
    event : l'evenement de clic
    x1 : la coordonnee X lors du clic sur le canvas
    y1 : la coordonnee Y lors du clic sur le canvas
    nbx : la cellule ou le clic a ete effectue (sa coordonnee X de debut sur le canvas)
    nby : la cellule ou le clic a ete effectue (sa coordonnee Y de debut sur le canvas)
    """
    x1 = event.x
    y1 = event.y
    nbx = x1 // cell_size
    nby = y1 // cell_size
    if rectangle_color[nbx][nby] == "green":
        canvas.itemconfig(rectangle[nbx][nby], fill='red')
        rectangle_color[nbx][nby] = 'red'



def temps(rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas):
    """
    Permet d'organiser les 3 temps d'un tour de l'automate.
    D'abord la mise en feu de parcelles foret si necessaire, ensuite l'extinction des anciennes parcelles de feu, et enfin la veritable allumage des nouvelles parcelles de feu
    """
    couleur("vert",rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)
    couleur("rouge",rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)
    couleur("orange",rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)


def couleur(couleur_test,rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas):
    """
    Permet de changer l'etat et la couleur de chaque parcelle du canvas si necessaire
    largeur : la largeur du canvas (en terme de rectangles)
    longueur : la longueur du canvas (en terme de rectangles)
    regle2compteur : compteur utile pour la regle 2 de mise en feu des parcelles 
    rand : un nombre random utile a la probabilite d'avoir une parcelle foret
    probas : la probas, pour la regle 2, qu'une parcelle prenne feu
    """
    for largeur in range (0,cols):
            for longueur in range (0,rows):
                regle2compteur = 0
                """ dabord on passe du vert au orange si necessaire, intermediaire pour ne pas confondre avec les anciens feux"""
                if couleur_test == "vert":
                    if rectangle_color[largeur][longueur] == "green":
                        if regle == "1":
                            if largeur > 0:
                                if rectangle_color[largeur-1][longueur] == "red":
                                    canvas.itemconfig(rectangle[largeur][longueur], fill='orange')
                                    rectangle_color[largeur][longueur] = 'orange'
                            if largeur < cols-1:
                                if rectangle_color[largeur+1][longueur] == "red":
                                    canvas.itemconfig(rectangle[largeur][longueur], fill='orange')
                                    rectangle_color[largeur][longueur] = 'orange'
                            if longueur > 0 :
                                if rectangle_color[largeur][longueur-1] == "red":
                                    canvas.itemconfig(rectangle[largeur][longueur], fill='orange')
                                    rectangle_color[largeur][longueur] = 'orange'
                            if longueur < rows-1:
                                if rectangle_color[largeur][longueur+1] == "red":
                                    canvas.itemconfig(rectangle[largeur][longueur], fill='orange')
                                    rectangle_color[largeur][longueur] = 'orange'
                        
                        if regle == "2":
                            if largeur > 0:
                                if rectangle_color[largeur-1][longueur] == "red":
                                    regle2compteur = regle2compteur +1
                            if largeur < cols-1:
                                if rectangle_color[largeur+1][longueur] == "red":
                                    regle2compteur = regle2compteur +1
                            if longueur > 0 :
                                if rectangle_color[largeur][longueur-1] == "red":
                                    regle2compteur = regle2compteur +1
                            if longueur < rows-1:
                                if rectangle_color[largeur][longueur+1] == "red":
                                    regle2compteur = regle2compteur +1

                            rand = random.randint(0,100)
                            probas = 1-(1/(regle2compteur+1))
                            if rand/100 <= probas and regle2compteur != 0:
                                canvas.itemconfig(rectangle[largeur][longueur], fill='orange')
                                rectangle_color[largeur][longueur] = 'orange'
                            

                """du rouge on passe au gris"""
                if couleur_test == "rouge":
                    if rectangle_color[largeur][longueur] == "red":
                        canvas.itemconfig(rectangle[largeur][longueur], fill='grey')
                        rectangle_color[largeur][longueur] = 'grey'

                """du orange on passe au rouge"""
                if couleur_test == "orange":
                    if rectangle_color[largeur][longueur] == "orange":
                        canvas.itemconfig(rectangle[largeur][longueur], fill='red')
                        rectangle_color[largeur][longueur] = 'red'






