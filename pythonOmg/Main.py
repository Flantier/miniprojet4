from tkinter import *
from fonctions_pythonOmg import *
import datetime
import random
import sys


"""init des valeurs"""
rows = int(sys.argv[1])
cols = int(sys.argv[2])
cell_size = int(sys.argv[3])
afforestation = float(sys.argv[4])
temps_ms = int(sys.argv[5])
regle = sys.argv[6]

"""init du canvas"""
master = Tk()
master.maxsize(cols*cell_size,rows*cell_size) 
master.minsize(cols*cell_size,rows*cell_size)
canvas = Canvas(master, width=cols*cell_size, height=rows*cell_size)
canvas.pack()

"""init des tableaux de gestion des rectangles"""
rectangle = [[0 for x in range(rows)] for y in range(cols)]
rectangle_color = [[0 for x in range(rows)] for y in range(cols)]

"""init de la foret"""
init_foret(rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)

"""mettre le feu"""
def click_fire(event):
    fonction_clic(rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,event,canvas)
canvas.bind('<Button-1>', click_fire)

"""gestion du temps"""
def lol():
    temps(rows,cols,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)
    master.after(temps_ms,lol)
lol()

"""Balkany"""
img = PhotoImage(file='balkany.ppm')
def click_Balkany(event):
    for largeur in range (0,cols):
        for longueur in range (0,rows):
            canvas.create_image(largeur*cell_size + 14, longueur*cell_size + 14, image = img)
canvas.bind('<Button-2>', click_Balkany)


master.mainloop()